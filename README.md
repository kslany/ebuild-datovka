# Ebuilds for Datovka

This repository contains Gentoo ebuild files for Datovka.

## Notes Related to Installing the Ebuild

Navigate to an acceptable place in the portage tree hierarchy. Download the ebuild and compute manifest data.

``` shell
cd /usr/portage/net-libs/libdatovka
curl -OL https://gitlab.labs.nic.cz/kslany/ebuild-datovka/raw/master/libdatovka-0.1.2.ebuild
ebuild libdatovka-0.1.2.ebuild manifest

cd /usr/portage/app-misc/datovka/
curl -OL https://gitlab.labs.nic.cz/kslany/ebuild-datovka/raw/master/datovka-4.17.0.ebuild
ebuild datovka-4.17.0.ebuild manifest
```
